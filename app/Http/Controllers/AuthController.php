<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserLoginRequest;
use App\Http\Requests\User\UserRegisterRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;
use App\Models\User;
use Validator;
use JWTAuth;


class AuthController extends Controller
{
    protected $userService;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(UserService $userService) {

        $this->userService = $userService;

        $this->middleware('jwt.verify', ['except' => ['login','register']]);
    }

    public function register(UserRegisterRequest $request) {
        $credentials = $request->json()->all();

        $user = $this->userService->registerUser(array_merge(
            $credentials,
            ['password' => bcrypt($request->password)],
        ));

        $res = $user;

        return response()->json($res ,200);
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserLoginRequest $request){

        $credentials = $request->json()->all();
        $jwt_token = null;
        if (! $jwtToken = JWTAuth::attempt($credentials) ) {
            $res = [
            'status'=>0,
            'error'=>'Invalid email or password',
            ];
            return response()->json($res,401);
        }

        return $this->createNewToken($jwtToken);
    }
    
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $token = $request->header('Authorization');
        JWTAuth::invalidate($token);
    return response()->json([
                'status' => 1,
                'message' => 'User logged out successfully'
            ]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        if(!auth()->check()){
            $res = [
                'status' =>0,
                'error'=>"User Not authenticated",
            ];
            return response()->json($res,401);
        }
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser() {
        $res = [
            'status' => 1,
        ];
        $status = 200;
        //Access token from the request
        $jwtToken = JWTAuth::parseToken();
        
        if(!$user = $jwtToken->authenticate()){
           $res = [
            'status' => 0,
            'error'=>'User Not Found',
            ];
            $status = 400; 
        }else{
            $res = $user;
        }
        return response()->json($res,$status);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        $res = [
            'status' => 1,
            'Authorization' =>$token
        ];
        return response()->json($res,200);
    }

}