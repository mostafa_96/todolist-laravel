<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\PriorityService;
use App\Http\Requests\Priority\PriorityUpdateRequest;
use App\Http\Requests\Priority\PriorityCreateRequest;

use Exception;


class PriorityController extends Controller
{

    protected $priorityService;

    public function __construct(PriorityService $priorityService)
    {
        $this->priorityService = $priorityService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res = $this->priorityService->getAllPriorities();
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PriorityCreateRequest $request)
    {
        $reqBody = $request->json()->all();
       // print_r($reqBody);

        $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res['data'] = $this->priorityService->createPriority($reqBody);
            
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
            
        }

        return response()->json($res,$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res['data'] = $this->priorityService->getPriorityById($id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PriorityUpdateRequest $request, $id)
    {
        $reqBody = $request->json()->all();
        $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res['data'] = $this->priorityService->updatePriority($id,$reqBody);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res['data'] = $this->priorityService->deletePriority($id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }
}
