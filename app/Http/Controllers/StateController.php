<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\State\StateCreateRequest;
use App\Http\Requests\State\StateUpdateRequest;

use App\Services\StateService;
use InvalidArgumentException;
use Exception;

class StateController extends Controller
{

    protected $stateService;

    public function __construct(StateService $stateService)
    {
        //$this->middleware('jwt.verify');
        $this->stateService = $stateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res['data'] = $this->stateService->getAllStates();
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StateCreateRequest $request)
    {
        $reqBody = $request->json()->all();
        $res = [
            'status'=>1
        ];
        $status = 201;
        try {
            $res['data'] = $this->stateService->createState($reqBody);
        } catch (Exception $e) {
            //print_r("My message".$e->getMessage());
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
            return response()->json($res,400);            
        }

        return response()->json($res,$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res['data'] = $this->stateService->getStateById($id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StateUpdateRequest $request, $id)
    {
        $reqBody = $request->json()->all();
        $res = [
            'status'=>1
        ];
        $status = 200;

        try {
            $res['data'] = $this->stateService->updateState($id,$reqBody);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = [
            'status'=>1
        ];
        $status = 200;
        try {
            $res['data'] = $this->stateService->deleteState($id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }
}
