<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Task\TaskCreateRequest;
use App\Http\Requests\Task\TaskUpdateRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\AssignTask;
use App\Services\TaskService;
use Exception;
use JWTAuth;

class TaskController extends Controller
{


    protected $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;
        try {
            $res = $this->taskService->getAllTasks($user_id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }


    public function getTasksByState($user_id,$state_id){
        $res = [];
        try {
            $res = $this->taskService->getTasksByState($user_id,$state_id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
        }
        return response()->json($res,200);

    }


    public function getTasksAssignedToUser($user_id){
        $res = [];
        try {
            $res = $this->taskService->getTasksAssignedToUser($user_id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
        }
        return response()->json($res,200);
    }

    public function getUserTasks($user_id){
        $res = [];
        try {
            $res = $this->taskService->getUserTasks($user_id);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
        }
        return response()->json($res,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskCreateRequest $request)
    {
        $reqBody = $request->json()->all();
        $res = [
            'status'=>1
        ];

        $status = 201;

        try {
            $res = $this->taskService->createTask($reqBody);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage(),
            ];
            $status = 500;
        }

        $jwtToken = JWTAuth::parseToken();
        //get current user
        $from_user = $jwtToken->authenticate();
        //get the user to assign task to.
        $to_user = $this->taskService->getUser($res->id);
        $this->taskService->sendMail($to_user,$from_user,$res);
        return response()->json($res,$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res['data'] = $this->taskService->getTaskById($id);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskUpdateRequest $request, $id)
    {
        $reqBody = $request->json()->all();
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res = $this->taskService->updateTask($id,$reqBody);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        //get current user
        $from_user = JWTAuth::parseToken()->authenticate();
        //get the user to assign task to.
        $to_user = $this->taskService->getUser($id);
        //get updated task
        $task = $this->taskService->getTaskById($id);
        $this->taskService->sendMail($to_user,$from_user,$task);
        return response()->json($res,$status);
    }

    public function reassign($task_id,$user_id)
    {
        $res = [
            'status'=>1,
        ];
        $status = 200;
        //get current user
        $from_user = JWTAuth::parseToken()->authenticate();
        if(!$from_user->isManager()){
            $res = [
                'status' => 0,
                'error'=>"you are not authrized to reassign tasks",
            ];
            return response()->json($res,403);
        }
        $task = $this->taskService->getTaskById($task_id);
        $task_arr = (array)$task;
        $task_arr['user_id'] = $user_id;
        try {
            $res['data'] = $this->taskService->updateTask($task_id,$task_arr);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        
        //get the user to assign task to.
        $to_user = $this->taskService->getUser($task_id);
        //get updated task
        $task = $this->taskService->getTaskById($task_id);
        $this->taskService->sendMail($to_user,$from_user,$task);
        return response()->json($res,$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res['data'] = $this->taskService->deleteTask($id);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }


    public function getPriority($task_id){
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res['data'] = $this->taskService->getPriority($task_id);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    public function getState($task_id){
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res['data'] = $this->taskService->getState($task_id);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    public function getUser($task_id){
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res['data'] = $this->taskService->getUser($task_id);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }
}
