<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Services\UserService;
use Exception;

class UserController extends Controller
{


    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('jwt.verify', ['except' => ['getManagers']]);
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res = $this->userService->getAllUsers();
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    public function getManagers(Request $request)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res = $this->userService->getManagers();
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    public function getUsers(Request $request)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res = $this->userService->getUsers();
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $reqBody = $request->json()->all();
        $reqBody['password'] = bcrypt($reqBody['password']);
        $res = [
            'status'=>1
        ];

        $status = 201;

        try {
            $res = $this->userService->createUser($reqBody);
        } catch (Exception $e) {
            $res = [
                'status'=>0,
                'error'=>$e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res['data'] = $this->userService->getUserById($id);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }
 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $reqBody = $request->json()->all();
        $res = [
            'status'=>1
        ];

        $status = 200;
        $reqBody['password'] = bcrypt($request->password);
        try {
            $res = $this->userService->updateUser($id,$reqBody);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = [
            'status'=>1
        ];

        $status = 200;

        try {
            $res['data'] = $this->userService->deleteUser($id);
        } catch (Exception $e) {
            $res = [
                'status' => 0,
                'error' => $e->getMessage()
            ];
            $status = 500;
        }
        return response()->json($res,$status);
    }
}
