<?php

namespace App\Http\Requests\State;

use Illuminate\Foundation\Http\FormRequest;

class StateCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('create-state');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>['bail','required','max:200'],
            'description'=>['bail','required','max:200']
        ];
    }
}
