<?php

namespace App\Http\Requests\State;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;
use App\Models\State;
use JWTAuth;

class StateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('update-state');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>['bail','required','max:200'],
            'description'=>['bail','required','max:200']
        ];
    }

    /*public function messages()
    {
        return [
            'title.required' => 'title is required.',
            'description.required'  => 'description must be submitted'
        ];
    }*/

   /* public function failedAuthorization()
   {
      throw new AuthorizationException("You don't have the authority to update this post");
   }*/

}
