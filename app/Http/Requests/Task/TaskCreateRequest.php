<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskCreateRequest extends FormRequest
{ 
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create-task',$this['user_id']);
    }
 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>['required','max:200'],
            'description'=>['required','max:200'],
            'created_date'=>['required','date'],
            'due_date'=>['required','date'],
            'completed_date'=>['date','nullable'],
            'state_id'=>['required','integer','min:1','exists:states,id'],
            'priority_id'=>['required','integer','min:1',
                            'exists:priorities,id'],
            'user_id'=>['required','integer','min:1','exists:users,id'],
            'created_by'=>['required','integer','min:1','exists:users,id']

        ];
    }
}
