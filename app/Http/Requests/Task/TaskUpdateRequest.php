<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update-task',$this->route('id'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>['bail','required','max:200'],
            'description'=>['bail','required','max:200'],
            'created_date'=>['bail','required','date'],
            'due_date'=>['bail','required','date'],
            'completed_date'=>['date','nullable'],
            'state_id'=>['bail','required','integer','min:1'],
            'priority_id'=>['bail','required','integer','min:1'],
            'user_id'=>['bail','required','integer','min:1'],
            'created_by'=>['required','integer','min:1','exists:users,id']

        ];
    }
}
