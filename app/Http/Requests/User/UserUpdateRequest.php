<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;
use App\Models\User;
use JWTAuth;
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('update-user',$this->route('id') );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|between:2,200',
            'email' => 'required|email|max:200|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'mgr_id' => 'integer|nullable',
        ];
    }
}
