<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssignTask extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $mail;

    public function __construct($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->mail['from_email'],$this->mail['from_name'])
        ->markdown('emails.assign-task',[
                        'from_name' => $this->mail['from_name'],
                        'to_name' => $this->mail['to_name'],
                        'title' => $this->mail['title'],
                        'description' => $this->mail['description'],
                        'due_date' => $this->mail['due_date'],
                        ]);
    }
}
