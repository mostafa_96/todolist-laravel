<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
	protected $fillable = [
		'title',
		'description',
	];

    protected $table = 'priorities';
    public $timestamps = false;

    public function tasks(){
    	return $this->hasMany(Task::class,'priority_id');
    }
}
