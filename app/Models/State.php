<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{

	protected $table = "states";
    public $timestamps = false;

	protected $fillable = [
		'title',
		'description',
	];

    public function tasks(){
    	return $this->hasMany(Task::class,'state_id');
    }
    
}
