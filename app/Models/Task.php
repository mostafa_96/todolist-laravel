<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Priority;
use App\Models\State;
use App\Models\User;


class Task extends Model
{
	protected $table = 'tasks';
    public $timestamps = false;

	protected $fillable = [
		'title',
		'description',
		'created_date',
		'due_date',
		'completed_date',
		'state_id',
		'priority_id',
		'user_id',
		'created_by',
	];

    public function priority(){
    	return $this->belongsTo(Priority::class);
    }

    public function state(){
    	return $this->belongsTo(State::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
