<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Models\State' => 'App\Policies\StatePolicy',
         'App\Models\User' => 'App\Policies\UserPolicy',
         'App\Models\Task' => 'App\Policies\TaskPolicy',

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('create-state', 'App\Policies\StatePolicy@create');
        Gate::define('update-state', 'App\Policies\StatePolicy@update');

        Gate::define('create-user', 'App\Policies\UserPolicy@create');
        Gate::define('update-user', 'App\Policies\UserPolicy@update');

        Gate::define('create-task', 'App\Policies\TaskPolicy@create');
        Gate::define('update-task', 'App\Policies\TaskPolicy@update');

    }
}
