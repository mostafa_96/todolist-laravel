<?php

namespace App\Repositories;

use App\Models\Priority;

class PriorityRepository
{
	protected $priority;
	
	function __construct(Priority $priority)
	{
		$this->priority = $priority;
	}

	public function getAllPriorities(){
		return $this->priority->all();
	}

	public function getPriorityById($id){
		return $this->priority->find($id);
	}

	public function createPriority($attributes){
		return $this->priority->create($attributes);
	}

	public function updatePriority($id,$attributes){
		return $this->priority->find($id)->update($attributes);
	}

	public function deletePriority($id){
		return $this->priority->find($id)->delete();
	}
}