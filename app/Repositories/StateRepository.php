<?php

namespace App\Repositories;

use App\Models\State;
use ModelNotFoundException;




class StateRepository
{
	
	protected $state;
	
	function __construct(State $state)
	{
		$this->state = $state;
	}

	public function getAllStates(){
		return $this->state->all();
	}

	public function getStateById($id){
		return $this->state->findOrFail($id);
	}

	public function createState($attributes){
		return $this->state->create($attributes);
	}

	public function updateState($id,$attributes){
		$stateToUpdate = $this->state->findOrFail($id);
		return $stateToUpdate->update($attributes);
	}

	public function deleteState($id){
		$stateToDelete = $this->state->findOrFail($id);
		return $stateToDelete->delete();
	}
}