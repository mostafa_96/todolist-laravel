<?php

namespace App\Repositories;

use App\Models\Task;
use Illuminate\Support\Facades\DB;


class TaskRepository
{
	
	protected $task;
	
	function __construct(Task $task)
	{
		$this->task = $task;
	}

	public function getAllTasks($user_id){
		return $this->task->where('user_id','=',$user_id)->paginate(3);
	}

	public function getTasksByState($user_id,$state_id){
		return $this->task->where('user_id', '=' ,$user_id)
			->where('state_id', '=' ,$state_id)->paginate(3);
	}

	public function getTasksAssignedToUser($user_id){
		return $this->task->where('user_id', '=' ,$user_id)
		->where('created_by','!=',$user_id)
		->paginate(3);
	}

	public function getUserTasks($user_id){
		return $this->task->where('user_id', '=' ,$user_id)
		->where('created_by','=',$user_id)
		->paginate(3);
	}

	public function getTaskById($id){
		return $this->task->findOrFail($id);
	}

	public function createTask($attributes){
		return $this->task->create($attributes);
	}

	public function updateTask($id,$attributes){
		$taskToUpdate = $this->task->findOrFail($id);
		return $taskToUpdate->update($attributes);
	}

	public function deleteTask($id){
		$taskToDelete = $this->task->findOrFail($id);
		return $taskToDelete->delete();
	}

	public function getPriority($task_id){
		$taskPriority = $this->task->findOrFail($task_id);
		return $taskPriority->priority;
	}

	public function getState($task_id){
		$taskState = $this->task->findOrFail($task_id);
		return $taskState->state;
	}

	public function getUser($task_id){
		$taskUser = $this->task->findOrFail($task_id);
		return $taskUser->user;
	}
}