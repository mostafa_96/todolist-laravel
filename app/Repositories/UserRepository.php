<?php

namespace App\Repositories;

use App\Models\User;



class UserRepository
{
	
	protected $user;
	
	function __construct(User $user)
	{
		$this->user = $user;
	}

	public function getAllUsers(){
		return $this->user->all();
	}

	public function getManagers(){
		return $this->user->where('mgr_id', '=' ,null)->get();
	}

	public function getUsers(){
		return $this->user
		->where('mgr_id','!=',null)
		->get();
	}

	public function getUserById($id){
		return $this->user->findOrFail($id);
	}

	public function createUser($attributes){
		return $this->user->create($attributes);
	}

	public function updateUser($id,$attributes){
		$userToUpdate = $this->user->findOrFail($id);
		return $userToUpdate->update($attributes);
	}

	public function deleteUser($id){
		return $this->user->findOrFail($id)->delete();
	}
}