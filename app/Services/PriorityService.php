<?php

namespace App\Services;
use App\Repositories\PriorityRepository;
use Validator;
use Exception;

class PriorityService
{
	
	protected $priorityRepository;
	function __construct(PriorityRepository $priorityRepository)
	{
		$this->priorityRepository = $priorityRepository;
	}

	public function getAllPriorities(){
		return $this->priorityRepository->getAllPriorities();
	}

	public function getPriorityById($id){
		return $this->priorityRepository->getPriorityById($id);
	}

	public function createPriority($attributes){
		return $this->priorityRepository->createPriority($attributes);
	}

	public function updatePriority($id,$attributes){
		return $this->priorityRepository->updatePriority($id,$attributes);
	}

	public function deletePriority($id){
		return $this->priorityRepository->deletePriority($id);
	}
}