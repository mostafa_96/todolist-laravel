<?php


namespace App\Services;

use App\Repositories\StateRepository;
use InvalidArgumentException;
use Validator;
use Exception;
class StateService
{
	
	protected $stateRepository;
	function __construct(StateRepository $stateRepository)
	{
		$this->stateRepository = $stateRepository;
	}

	public function getAllStates(){
		return $this->stateRepository->getAllStates();
	}

	public function getStateById($id){
		$validator = Validator::make(['id'=>$id] ,[
			'id' => 'required|integer|min:1'
		]);
		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->stateRepository->getStateById($id);
	}

	public function createState($attributes){
		return $this->stateRepository->createState($attributes);
	}

	public function updateState($id,$attributes){

		$validator = Validator::make(['id'=>$id],[
			'id' => 'required|integer|min:1'
		]);
		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->stateRepository->updateState($id,$attributes);
	}


	public function deleteState($id){
		$validator = Validator::make(['id'=>$id],[
			'id' => 'required|integer|min:1'
		]);
		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->stateRepository->deleteState($id);
	}
}