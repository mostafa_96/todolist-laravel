<?php


namespace App\Services;

use App\Repositories\TaskRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\AssignTask;
use App\Jobs\SendEmailJob;
use Validator;
use Exception;
class TaskService
{
	
	protected $taskRepository;
	function __construct(TaskRepository $taskRepository)
	{
		$this->taskRepository = $taskRepository;
	}

	public function getAllTasks($user_id){
		return $this->taskRepository->getAllTasks($user_id);
	}

	public function getTasksByState($user_id,$state_id){
		return $this->taskRepository->getTasksByState($user_id,$state_id);

	}
	public function getTasksAssignedToUser($user_id){
		return $this->taskRepository->getTasksAssignedToUser($user_id,);
	}
	
	public function getUserTasks($user_id){
		return $this->taskRepository->getUserTasks($user_id,);
	}


	public function getTaskById($id){
		/*$validator = Validator::make(['id'=>$id] ,[
			'id' => 'required|integer|min:1'
		]);

		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}*/
		return $this->taskRepository->getTaskById($id);
	}

	public function createTask($attributes){
		return $this->taskRepository->createTask($attributes);
	}

	public function updateTask($id,$attributes){
		$validator = Validator::make(['id'=>$id] ,[
			'id' => 'required|integer|min:1'
		]);

		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}

		return $this->taskRepository->updateTask($id,$attributes);
	}

	public function deleteTask($id){
		$validator = Validator::make(['id'=>$id] ,[
			'id' => 'required|integer|min:1'
		]);

		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->taskRepository->deleteTask($id);
	}

	public function getPriority($task_id){
		$validator = Validator::make(['id'=>$task_id] ,[
			'id' => 'required|integer|min:1'
		]);

		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->taskRepository->getPriority($task_id);
	}

	public function getState($task_id){
		$validator = Validator::make(['id'=>$task_id] ,[
			'id' => 'required|integer|min:1'
		]);

		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->taskRepository->getState($task_id);
	}

	public function getUser($task_id){
		$validator = Validator::make(['id'=>$task_id] ,[
			'id' => 'required|integer|min:1'
		]);

		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->taskRepository->getUser($task_id);
	}

	public function sendMail($user,$manager,$task){
		if($user->id == $manager->id) return;
		$mail = [
			'from_name' => $manager->name,
			'from_email' => $manager->email,
			'to_name' => $user->name,
			'to_email' => $user->email,
			'title' => $task->title,
			'description' => $task->description,
			'due_date' => $task->due_date
		];
		$mialJob = (new SendEmailJob($mail))
            ->delay(
            	now()
            	->addSeconds(5)
            ); 
        dispatch($mialJob);

        //echo "task mail send successfully in the background...";
	}
}