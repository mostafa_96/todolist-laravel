<?php


namespace App\Services;

use App\Repositories\UserRepository;
use Validator;
use Exception;

class UserService
{
	
	protected $userRepository;
	function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public function getAllUsers(){
		return $this->userRepository->getAllUsers();
	}

	public function getManagers(){
		return $this->userRepository->getManagers();
	}

	public function getUsers(){
		return $this->userRepository->getUsers();
	}

	public function getUserById($id){

		$validator = Validator::make(['id'=>$id] ,[
			'id' => 'required|integer|exists:users'
		]);
		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		$user = $this->userRepository->getUserById($id);
		return $user;
	}

	public function registerUser($attributes){
		$user = $this->getUserById($attributes['mgr_id']);
		if(!$user->isManager()){
			throw new Exception("mgr_id is not a manager");
		}
		return $this->userRepository->createUser($attributes);
	}

	public function createUser($attributes){
		return $this->userRepository->createUser($attributes);
	}

	public function updateUser($id,$attributes){

		$currentUser = \JWTAuth::parseToken()->authenticate();
		if(!$currentUser->isManager()){
			if(!array_key_exists('mgr_id',$attributes) || is_null($attributes['mgr_id'])){
				throw new Exception("you are not authorized to be a manager");
			}

			$mgr = $this->getUserById($attributes['mgr_id']);
			if(!$mgr->isManager()){
				throw new Exception("mgr_id is not a manager");
			}
		}

		return $this->userRepository->updateUser($id,$attributes);
	}

	public function deleteUser($id){
		$validator = Validator::make(['id'=>$id] ,[
			'id' => 'required|integer|exists:users'
		]);
		if($validator->fails()){
			throw new Exception($validator->errors()->first());
		}
		return $this->userRepository->deleteUser($id);
	}
}