@component('mail::message')
# Task Assignment
hello {{$to_name}}, <br>
A new task is assigned to you by {{$from_name}}<br>
Task Title : {{$title}}<br>
Task Description : {{$description}}<br>
Due Date : {{$due_date}}<br>

@endcomponent
