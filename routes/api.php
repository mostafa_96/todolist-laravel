<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\PriorityController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\AuthController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Authentication route group

Route::group([
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/me', [AuthController::class, 'getAuthenticatedUser']);  

});

//tasks route group
Route::group([
    'middleware' => 'jwt.verify',
    'prefix' => 'tasks'

], function ($router) {
    Route::post('/', [TaskController::class, 'store']);
    Route::put('/{id}', [TaskController::class, 'update']);
    Route::put('/reassign/{task_id}/{user_id}',[TaskController::class,'reassign']); 
    Route::get('/{id}', [TaskController::class, 'show']);
    Route::get('/all/{user_id}', [TaskController::class, 'index']); 
    Route::get('/assigned/{user_id}', [TaskController::class, 'getTasksAssignedToUser']); 
    Route::get('/mytasks/{user_id}', [TaskController::class, 'getUserTasks']); 
    Route::get('/bystate/{user_id}/{state_id}',[TaskController::class,'getTasksByState']);

    Route::get('/priority/{task_id}',[TaskController::class,'getPriority']);
	Route::get('/state/{task_id}',[TaskController::class,'getState']);
	Route::get('/user/{task_id}',[TaskController::class,'getUser']); 
	
    Route::delete('/{id}', [TaskController::class, 'destroy']);    
});

//user route group
Route::group([
    'prefix' => 'users'

], function ($router) {
    Route::post('/', [UserController::class, 'store']);
    Route::put('/{id}', [UserController::class, 'update']);
    Route::get('/{id}', [UserController::class, 'show']);
    Route::get('/', [UserController::class, 'index']);
    Route::get('/managers', [UserController::class, 'getManagers']);
    Route::get('/users', [UserController::class, 'getUsers']);
    Route::delete('/{id}', [UserController::class, 'destroy']);    
});


//states route group
Route::group([
    'middleware' => 'jwt.verify',
    'prefix' => 'states'

], function ($router) {
    Route::post('/', [StateController::class, 'store']);
    Route::put('/{id}', [StateController::class, 'update']);
    Route::get('/{id}', [StateController::class, 'show']);
    Route::get('/', [StateController::class, 'index']);
    Route::delete('/{id}', [StateController::class, 'destroy']);    
});

//priorities route group
Route::group([
    'middleware' => 'jwt.verify',
    'prefix' => 'priorities'

], function ($router) {
    Route::post('/', [PriorityController::class, 'store']);
    Route::put('/{id}', [PriorityController::class, 'update']);
    Route::get('/{id}', [PriorityController::class, 'show']);
    Route::get('/', [PriorityController::class, 'index']);
    Route::delete('/{id}', [PriorityController::class, 'destroy']);    
});


